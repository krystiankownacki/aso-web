var emailValue;
var passwordValue;

$('#login').bind('click', function () {
    emailValue = $("[name='email']").val();
    passwordValue = $("[name='password']").val();

    $.ajax({
        type: 'POST',
        url: 'https://aso-web.herokuapp.com/login/',
        dataType: 'json',
        contentType: 'application/json',
        data: JSON.stringify({username: emailValue, password: passwordValue}),
        success: function (data, textStatus, jQxhr) {
            console.log('login succcess');
            var json = JSON.parse(JSON.stringify(data));
            var id = json.id;
            window.location.href = 'https://aso-web.herokuapp.com/index.html?id='+id;
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
            console.log('login failed');
        }
    });
});
