var child_width;
var child_length;
var map;
var childId;
var parentId;
var childSource = new ol.source.Vector();
var routeSource = new ol.source.Vector();
var routePoints = [];
var currentPositionStyle = new ol.style.Style({
    image: new ol.style.Icon({
        color: [255, 0, 0],
        crossOrigin: 'anonymous',
        src: 'https://openlayers.org/en/v4.1.1/examples/data/dot.png'
    })
});
var routeStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: [255, 0, 0],
        width: 4
    })
});

function init() {
    parentId = $.urlParam();
    setChildren();
    childId = $('#mySidenav a:nth-child(3)').attr('id');
    getCurrentPosition();
    setMap();
    openNav();
    closeNav();
    setChildId();
}

$.urlParam = function(){
        var results = new RegExp('[\?&]id=([^&#]*)').exec(window.location.href);
        if (results==null){
            return null;
        }
        else{
            console.log(decodeURI(results[1]) || 0);
            return decodeURI(results[1]) || 0
        }
};

function getCurrentPosition() {
    $.ajax({
        type: "GET",
        url: "https://aso-web.herokuapp.com/coordinates/child/" + childId,
        dataType: "json",
        async: false,
        success: function (response) {
            console.log('Connected');
            var json = JSON.parse(JSON.stringify(response));
            var coordinates = json;
            var coordinatesLength = coordinates.length;

            //ostatnie wspolrzedne
            console.log(coordinates[coordinatesLength - 1].width + " " + coordinates[coordinatesLength - 1].length);
            child_width = coordinates[coordinatesLength - 1].width; //szerokosc latitude
            child_length = coordinates[coordinatesLength - 1].length; //dlugosc longitude

            var childFeature = new ol.Feature({
                geometry: new ol.geom.Point(ol.proj.fromLonLat([parseFloat(child_length), parseFloat(child_width)]))
            });
            childFeature.setStyle(currentPositionStyle);
            childSource.clear();
            childSource.addFeature(childFeature);

            //Last location data
            var time = coordinates[coordinatesLength - 1].time;
            var date = coordinates[coordinatesLength - 1].date;
            $("p").text('Last location time: ' + time + ' date: ' + date);

            //rysowanie drogi
            routePoints.length = 0;
            for (var i = 0; i < coordinatesLength; i++) {
                var point = [parseFloat(coordinates[i].length), parseFloat(coordinates[i].width)]; //longitude, latitude
                routePoints.push(point);
            }
            for (var i = 0; i < routePoints.length; i++) {
                routePoints[i] = ol.proj.fromLonLat(routePoints[i])
            }
            var routeFeature = new ol.Feature({
                geometry: new ol.geom.LineString(routePoints)
            });
            routeSource.clear();
            routeFeature.setStyle(routeStyle);
            routeSource.addFeature(routeFeature);
        },
        error: function (e) {
            console.log("getCurrentPosition request error " + e);
            console.log("Not connected");
        }
    });
}

function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
}

function setMap() {
    var osmMap = new ol.layer.Tile({
        source: new ol.source.OSM()
    });

    var childLayer = new ol.layer.Vector({source: childSource});
    var routeLayer = new ol.layer.Vector({source: routeSource});

    map = new ol.Map({
        target: 'map',
        layers: [osmMap, childLayer, routeLayer],
        view: new ol.View({
            center: ol.proj.fromLonLat([parseFloat(child_length), parseFloat(child_width)]),
            zoom: 13
        })
    });
}

function setChildren() {
    $.ajax({
        type: "GET",
        url: "https://aso-web.herokuapp.com/parent/"+parentId,
        dataType: "json",
        async: false,
        success: function (response) {
            console.log('Connected');
            var json = JSON.parse(JSON.stringify(response));
            var children = json;

            for (var i = 0; i < children.length; i++) {
                $('#mySidenav').append('<a id=' + children[i].id + '>' + children[i].firstName + ' ' + children[i].lastName + '</a>');
            }
        },
        error: function (e) {
            console.log("setChildren request error " + e);
            console.log("Not connected");
        }
    });
}

function setChildId() {
    $('#mySidenav').children('a').not('.closebtn').click(function () {
        childId = this.id;
        childSource.clear();
        routeSource.clear();
        console.log(childId);
    });
}

function addChildFunction() {
    keyValue = $("[name='keyField']").val();
    $.ajax({
        type: 'POST',
        url: 'https://aso-web.herokuapp.com/keygen/addChild/' + keyValue + '/'+parentId,
        dataType: 'json',
        contentType: 'application/json',
        success: function (data) {
            console.log('add child succcess');
            setChildren();
        },
        error: function (jqXhr, textStatus, errorThrown) {
            console.log(errorThrown);
            console.log('add child failed');
        }
    });
}

