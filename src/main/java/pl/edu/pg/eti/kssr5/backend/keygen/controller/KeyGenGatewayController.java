package pl.edu.pg.eti.kssr5.backend.keygen.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pg.eti.kssr5.backend.keygen.dto.KeyGenDto;
import pl.edu.pg.eti.kssr5.backend.keygen.endpoint.KeyGenGateway;
import pl.edu.pg.eti.kssr5.backend.keygen.service.KeyGenService;

@CrossOrigin
@RestController
public class KeyGenGatewayController implements KeyGenGateway {

    @Autowired
    KeyGenService keyGenService;

    @Override
    public Long create(@RequestBody KeyGenDto keyGenDto) {
        return keyGenService.create(keyGenDto);
    }

    @Override
    public void addChildToParent(@PathVariable("key") String key, @PathVariable("id") Long parentId) {
        keyGenService.addChildToParent(key, parentId);
    }
}
