package pl.edu.pg.eti.kssr5.backend.keygen.model.jpa;

import lombok.Data;
import pl.edu.pg.eti.kssr5.backend.child.model.jpa.Child;
import pl.edu.pg.eti.kssr5.backend.core.dao.AbstractEntity;

import javax.persistence.*;

@Data
@Entity
@Table(name = "KEY_GENERATOR")
public class KeyGen extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "KEY_GEN_ID", nullable = false)
    private Long id;

    @Column(name = "KEY_GEN", unique = true, nullable = false)
    private String key;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CHILD_ID")
    private Child child;
}
