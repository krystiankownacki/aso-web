package pl.edu.pg.eti.kssr5.backend.keygen.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class KeyGenDto implements Serializable {
    private String key;
    private Long childId;
}
