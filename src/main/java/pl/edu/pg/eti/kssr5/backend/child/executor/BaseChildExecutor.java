package pl.edu.pg.eti.kssr5.backend.child.executor;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.pg.eti.kssr5.backend.child.dao.ChildRepository;
import pl.edu.pg.eti.kssr5.backend.userprofile.dao.UserProfileRepository;

public abstract class BaseChildExecutor {

    @Autowired
    ChildRepository repository;

    @Autowired
    UserProfileRepository userProfileRepository;

    ModelMapper mapper = new ModelMapper();
}
