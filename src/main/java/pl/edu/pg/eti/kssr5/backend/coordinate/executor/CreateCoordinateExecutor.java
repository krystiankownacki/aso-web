package pl.edu.pg.eti.kssr5.backend.coordinate.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.coordinate.dto.CoordinateDto;
import pl.edu.pg.eti.kssr5.backend.coordinate.model.jpa.Coordinate;

import java.time.LocalDate;
import java.time.LocalTime;

@Component
public class CreateCoordinateExecutor extends BaseCoordinateExecutor{

    public CoordinateDto save(CoordinateDto coordinateDto) {
        Coordinate entity = modelMapper.map(coordinateDto, Coordinate.class);
        entity.setDate(LocalDate.parse(coordinateDto.getDate()));
        entity.setTime(LocalTime.parse(coordinateDto.getTime()));
        return modelMapper.map(coordinateRepository.save(entity), CoordinateDto.class);
    }
}
