package pl.edu.pg.eti.kssr5.backend.coordinate.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CoordinateDto implements Serializable {
    private Long id;
    private String length;
    private String width;
    private String date;
    private String time;
    private Long childId;
}
