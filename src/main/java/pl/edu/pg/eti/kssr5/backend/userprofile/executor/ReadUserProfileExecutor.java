package pl.edu.pg.eti.kssr5.backend.userprofile.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.LoginDto;
import pl.edu.pg.eti.kssr5.backend.userprofile.model.jpa.UserProfile;

import java.util.List;

@Component
public class ReadUserProfileExecutor extends BaseUserProfileExecutor {

    public LoginDto userAuthenticate(LoginDto loginDto){
        List<UserProfile> users = repository.findByUsernameAndPassword(loginDto.getUsername(), loginDto.getPassword());
        LoginDto login = users.stream().map(userProfile -> modelMapper.map(userProfile, LoginDto.class)).findFirst().orElse(null);
        if(login != null){
            if(login.getIsParent()){
                login.setId(parentRepository.findByActiveFlagTrueAndUser_Username(loginDto.getUsername()).getId());
            } else {
                login.setId(childRepository.findByActiveFlagTrueAndUser_Username(loginDto.getUsername()).getId());
            }
        }
        return login;
    }
}
