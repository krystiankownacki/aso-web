package pl.edu.pg.eti.kssr5.backend.userprofile.model.jpa;

import lombok.Data;
import pl.edu.pg.eti.kssr5.backend.core.dao.AbstractEntity;

import javax.persistence.*;

@Data
@Entity
@Table(name = "USER_PROFILE")
public class UserProfile extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "USER_PROFILE_ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "USERNAME", unique = true)
    private String username;

    @Column(name = "PASSWORD")
    private String password;

    @Column(name = "IS_PARENT")
    private Boolean isParent;
}
