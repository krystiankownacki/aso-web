package pl.edu.pg.eti.kssr5.backend.userprofile.endpoint;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.LoginDto;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.UserDto;

@RequestMapping("/login")
public interface UserProfileGateway {

    @RequestMapping(method = RequestMethod.POST, value = "/")
    LoginDto userAuthenticate(@RequestBody LoginDto loginDto);

    @RequestMapping(method = RequestMethod.POST, value = "/register")
    Long registerUser(@RequestBody UserDto userDto);
}
