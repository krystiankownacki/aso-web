package pl.edu.pg.eti.kssr5.backend.userprofile.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class LoginDto implements Serializable{
    private String username;
    private String password;
    private Boolean isParent;
    private Long id;
}
