package pl.edu.pg.eti.kssr5.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LoginController {

    @GetMapping(value = "/")
    public String openLoginPage() {
        return "login";
    }
}
