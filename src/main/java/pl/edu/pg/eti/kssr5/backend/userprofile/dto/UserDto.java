package pl.edu.pg.eti.kssr5.backend.userprofile.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UserDto implements Serializable{
    private Long id;
    private String username;
    private String password;
    private Boolean isParent;
    private String firstName;
    private String lastName;
    private Long personId;
}
