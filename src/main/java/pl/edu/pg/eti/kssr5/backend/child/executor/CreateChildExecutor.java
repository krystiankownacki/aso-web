package pl.edu.pg.eti.kssr5.backend.child.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.child.dto.ChildDto;
import pl.edu.pg.eti.kssr5.backend.child.model.jpa.Child;

@Component
public class CreateChildExecutor extends BaseChildExecutor {

    public ChildDto create(ChildDto childDto, Long userId){
        Child entity = mapper.map(childDto, Child.class);
        entity.setUser(userProfileRepository.getOne(userId));
        return mapper.map(repository.save(entity), ChildDto.class);
    }
}
