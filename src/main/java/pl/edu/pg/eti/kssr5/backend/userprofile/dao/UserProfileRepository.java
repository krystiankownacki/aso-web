package pl.edu.pg.eti.kssr5.backend.userprofile.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pg.eti.kssr5.backend.userprofile.model.jpa.UserProfile;

import java.util.List;

@Repository
public interface UserProfileRepository extends JpaRepository<UserProfile, Long> {

    List<UserProfile> findByUsernameAndPassword(String username, String password);
}
