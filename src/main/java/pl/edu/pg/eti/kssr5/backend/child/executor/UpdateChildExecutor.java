package pl.edu.pg.eti.kssr5.backend.child.executor;

import org.springframework.stereotype.Component;

@Component
public class UpdateChildExecutor extends BaseChildExecutor{

    public void addParentToChild(Long parentId, Long childId){
        repository.addParentToChildId(parentId, childId);
    }
}
