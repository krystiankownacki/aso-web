package pl.edu.pg.eti.kssr5.backend.keygen.executor;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public class KeyGenExecutor {

    @Autowired
    ReadKeyGenExecutor readExecutor;

    @Autowired
    CreateKeyGenExecutor createExecutor;
}
