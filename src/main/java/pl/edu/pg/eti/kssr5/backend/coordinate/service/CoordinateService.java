package pl.edu.pg.eti.kssr5.backend.coordinate.service;

import pl.edu.pg.eti.kssr5.backend.coordinate.dto.CoordinateDto;

import java.util.List;

public interface CoordinateService {

    List<CoordinateDto> findCoordinatesByChildId(Long childId);

    CoordinateDto save(CoordinateDto coordinateDto);
}
