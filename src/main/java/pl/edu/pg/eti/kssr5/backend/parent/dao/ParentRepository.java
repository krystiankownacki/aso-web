package pl.edu.pg.eti.kssr5.backend.parent.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pg.eti.kssr5.backend.parent.model.jpa.Parent;

@Repository
public interface ParentRepository extends CrudRepository<Parent, Long> {

	Parent findParentByActiveFlagTrueAndId(Long id);

	Parent findByActiveFlagTrueAndUser_Username(String username);
}
