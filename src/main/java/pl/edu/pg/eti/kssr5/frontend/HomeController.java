package pl.edu.pg.eti.kssr5.frontend;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class HomeController {
	
	@GetMapping(value = "/index")
	public String openHomePage() {
		return "index";
	}

}
