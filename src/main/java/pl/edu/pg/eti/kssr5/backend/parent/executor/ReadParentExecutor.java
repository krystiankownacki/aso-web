package pl.edu.pg.eti.kssr5.backend.parent.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.parent.dto.ParentDto;

@Component
public class ReadParentExecutor extends BaseParentExecutor{

    public ParentDto getParent(Long parentId){
        return mapper.map(repository.findParentByActiveFlagTrueAndId(parentId), ParentDto.class);
    }

}
