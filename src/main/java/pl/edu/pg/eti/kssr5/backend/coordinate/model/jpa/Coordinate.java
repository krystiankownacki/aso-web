package pl.edu.pg.eti.kssr5.backend.coordinate.model.jpa;

import lombok.Data;
import pl.edu.pg.eti.kssr5.backend.child.model.jpa.Child;
import pl.edu.pg.eti.kssr5.backend.core.converter.LocalDateToStringConverter;
import pl.edu.pg.eti.kssr5.backend.core.converter.LocalTimeToStringConverter;
import pl.edu.pg.eti.kssr5.backend.core.dao.AbstractEntity;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

/**
 * Created by Krystian on 2017-06-06.
 */
@Data
@Entity
@Table(name = "COORDINATES")
public class Coordinate extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(unique = true)
    private Long id;

    @Column(name = "LENGTH", precision = 8)
    private String length;

    @Column(name = "WIDTH", precision = 8)
    private String width;

    @Column(name = "DATE")
    @Convert(converter = LocalDateToStringConverter.class)
    private LocalDate date;

    @Column(name = "TIME")
    @Convert(converter = LocalTimeToStringConverter.class)
    private LocalTime time;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "CHILD_ID")
    private Child child;
}
