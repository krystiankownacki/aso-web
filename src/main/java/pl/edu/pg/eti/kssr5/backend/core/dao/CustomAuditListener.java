package pl.edu.pg.eti.kssr5.backend.core.dao;

import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import java.time.LocalDate;

public class CustomAuditListener {

    @PrePersist
    void onInsert(Object o){
        if(o instanceof AbstractEntity) {
            ((AbstractEntity) o).setDateCreate(LocalDate.now());
            ((AbstractEntity) o).setActiveFlag(Boolean.TRUE);
            ((AbstractEntity) o).setDescription("");
            ((AbstractEntity) o).setLastModifiedDate(LocalDate.now());
        }
    }

    @PreUpdate
    void onUpdate(Object o){
        if(o instanceof AbstractEntity){
         ((AbstractEntity) o).setLastModifiedDate(LocalDate.now());
        }
    }
}
