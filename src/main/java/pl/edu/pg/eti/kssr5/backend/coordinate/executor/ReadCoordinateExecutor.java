package pl.edu.pg.eti.kssr5.backend.coordinate.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.coordinate.dto.CoordinateDto;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReadCoordinateExecutor extends BaseCoordinateExecutor {

    public List<CoordinateDto> getCoordinatesByChildId(Long childId){
        return coordinateRepository.findAllByChild_Id(childId).stream().map(coordinate -> modelMapper.map(coordinate, CoordinateDto.class)).collect(Collectors.toList());
    }
}
