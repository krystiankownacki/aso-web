package pl.edu.pg.eti.kssr5.backend.parent.model.jpa;

import lombok.Data;
import pl.edu.pg.eti.kssr5.backend.child.model.jpa.Child;
import pl.edu.pg.eti.kssr5.backend.core.dao.AbstractEntity;
import pl.edu.pg.eti.kssr5.backend.userprofile.model.jpa.UserProfile;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Krystian on 2017-06-06.
 */
@Data
@Entity
@Table(name = "PARENT")
public class Parent extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "PARENT_ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @OneToMany(cascade = {CascadeType.REFRESH, CascadeType.REMOVE}, mappedBy = "parent")
    private List<Child> childs;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "USER", referencedColumnName = "USERNAME")
    private UserProfile user;
}
