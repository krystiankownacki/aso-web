package pl.edu.pg.eti.kssr5.backend.keygen.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.child.dto.ChildDto;

@Component
public class ReadKeyGenExecutor extends BaseKeyGenExecutor{

    public ChildDto getChildByKenGen(String key){
        return modelMapper.map(keyGenRepository.findByKeyAndActiveFlagTrue(key).getChild(), ChildDto.class);
    }
}
