package pl.edu.pg.eti.kssr5.backend.coordinate.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pg.eti.kssr5.backend.coordinate.model.jpa.Coordinate;

import java.util.List;

/**
 * Created by Krystian on 2017-06-06.
 */
@Repository
public interface CoordinateRepository extends CrudRepository<Coordinate, Long>{

    List<Coordinate> findAllByChild_Id(Long childId);

}
