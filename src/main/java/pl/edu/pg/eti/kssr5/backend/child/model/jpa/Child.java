package pl.edu.pg.eti.kssr5.backend.child.model.jpa;

import lombok.Data;
import pl.edu.pg.eti.kssr5.backend.core.dao.AbstractEntity;
import pl.edu.pg.eti.kssr5.backend.parent.model.jpa.Parent;
import pl.edu.pg.eti.kssr5.backend.userprofile.model.jpa.UserProfile;

import javax.persistence.*;

/**
 * Created by Krystian on 2017-06-06.
 */
@Data
@Entity
@Table(name = "CHILD")
public class Child extends AbstractEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "CHILD_ID", unique = true, nullable = false)
    private Long id;

    @Column(name = "FIRST_NAME")
    private String firstName;

    @Column(name = "LAST_NAME")
    private String lastName;

    @OneToOne(fetch = FetchType.EAGER, cascade = CascadeType.MERGE)
    @JoinColumn(name = "USER", referencedColumnName = "USERNAME")
    private UserProfile user;

    @ManyToOne(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "PARENT_ID")
    private Parent parent;
}
