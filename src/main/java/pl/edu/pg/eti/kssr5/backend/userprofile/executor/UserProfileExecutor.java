package pl.edu.pg.eti.kssr5.backend.userprofile.executor;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public class UserProfileExecutor {

    @Autowired
    ReadUserProfileExecutor readExecutor;

    @Autowired
    CreateUserProfileExecutor createExecutor;
}
