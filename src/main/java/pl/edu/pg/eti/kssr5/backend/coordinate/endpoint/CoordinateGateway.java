package pl.edu.pg.eti.kssr5.backend.coordinate.endpoint;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.pg.eti.kssr5.backend.coordinate.dto.CoordinateDto;

import java.util.List;

@RequestMapping("/coordinates")
public interface CoordinateGateway {

    @RequestMapping(method = RequestMethod.GET, value = "/child/{childId}")
    List<CoordinateDto> findAllCoordinatesByChild(@PathVariable Long childId);

    @RequestMapping(method = RequestMethod.POST, value = "/save")
    CoordinateDto postCoordinate(@RequestBody CoordinateDto coordinateDto);
}
