package pl.edu.pg.eti.kssr5.backend.parent.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pg.eti.kssr5.backend.child.dto.ChildDto;
import pl.edu.pg.eti.kssr5.backend.child.executor.ChildExecutor;
import pl.edu.pg.eti.kssr5.backend.parent.endpoint.ParentGateway;

import java.util.List;

@RestController
public class ParentGatewayController implements ParentGateway {

	@Autowired
    ChildExecutor childExecutor;

	@Override
	public List<ChildDto> getChildrens(@PathVariable Long parentId) {
		return childExecutor.getReadExecutor().getChildrensByParentId(parentId);
	}
}
