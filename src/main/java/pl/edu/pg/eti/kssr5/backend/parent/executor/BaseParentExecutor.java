package pl.edu.pg.eti.kssr5.backend.parent.executor;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.pg.eti.kssr5.backend.parent.dao.ParentRepository;
import pl.edu.pg.eti.kssr5.backend.userprofile.dao.UserProfileRepository;

public abstract class BaseParentExecutor {

    @Autowired
    ParentRepository repository;

    @Autowired
    UserProfileRepository userProfileRepository;

    ModelMapper mapper = new ModelMapper();
}
