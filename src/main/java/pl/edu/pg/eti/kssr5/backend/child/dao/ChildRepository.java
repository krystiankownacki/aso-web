package pl.edu.pg.eti.kssr5.backend.child.dao;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import pl.edu.pg.eti.kssr5.backend.child.model.jpa.Child;

import java.util.List;

@Repository
public interface ChildRepository extends CrudRepository<Child, Long> {

    List<Child> getAllByActiveFlagTrueAndParent_Id(Long parentId);

    @Modifying
    @Query(value = "UPDATE Child c SET c.parent.id = :parentId WHERE c.id = :childId")
    void addParentToChildId(@Param("parentId") Long parentId, @Param("childId") Long childId);

    Child findByActiveFlagTrueAndUser_Username(String username);
}
