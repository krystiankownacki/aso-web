package pl.edu.pg.eti.kssr5.backend.parent.endpoint;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.pg.eti.kssr5.backend.child.dto.ChildDto;

import java.util.List;

@RequestMapping(value = "/parent")
public interface ParentGateway {

	@RequestMapping(method = RequestMethod.GET, value = "/{parentId}")
	List<ChildDto> getChildrens(@PathVariable Long parentId);
}
