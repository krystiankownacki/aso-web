package pl.edu.pg.eti.kssr5.backend.userprofile.service.impl;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pg.eti.kssr5.backend.child.dto.ChildDto;
import pl.edu.pg.eti.kssr5.backend.child.executor.ChildExecutor;
import pl.edu.pg.eti.kssr5.backend.parent.dto.ParentDto;
import pl.edu.pg.eti.kssr5.backend.parent.executor.ParentExecutor;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.LoginDto;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.UserDto;
import pl.edu.pg.eti.kssr5.backend.userprofile.executor.UserProfileExecutor;
import pl.edu.pg.eti.kssr5.backend.userprofile.service.UserProfileService;

import javax.transaction.Transactional;

@Service
public class UserProfileServiceImpl implements UserProfileService {

    @Autowired
    UserProfileExecutor userProfileExecutor;

    @Autowired
    ParentExecutor parentExecutor;

    @Autowired
    ChildExecutor childExecuotr;

    ModelMapper mapper = new ModelMapper();

    @Override
    public LoginDto userAuthenticate(LoginDto loginDto) {
        return userProfileExecutor.getReadExecutor().userAuthenticate(loginDto);
    }

    @Override
    @Transactional
    public Long createUser(UserDto userToSave) {
        UserDto createdUser = userProfileExecutor.getCreateExecutor().createUser(userToSave);
        Long id;
        if(userToSave.getIsParent()){
            id = parentExecutor.getCreateExecutor().create(mapper.map(userToSave, ParentDto.class), createdUser.getId()).getId();
        } else {
            id = childExecuotr.getCreateExecutor().create(mapper.map(userToSave, ChildDto.class), createdUser.getId()).getId();
        }
        return id;
    }
}
