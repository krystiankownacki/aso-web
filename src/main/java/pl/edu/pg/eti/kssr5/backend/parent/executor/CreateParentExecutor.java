package pl.edu.pg.eti.kssr5.backend.parent.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.parent.dto.ParentDto;
import pl.edu.pg.eti.kssr5.backend.parent.model.jpa.Parent;

@Component
public class CreateParentExecutor extends BaseParentExecutor {

    public ParentDto create(ParentDto parentDto, Long userId){
        Parent entity = mapper.map(parentDto, Parent.class);
        entity.setUser(userProfileRepository.findOne(userId));
        return mapper.map(repository.save(entity), ParentDto.class);
    }
}
