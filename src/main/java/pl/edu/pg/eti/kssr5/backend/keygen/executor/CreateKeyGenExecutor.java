package pl.edu.pg.eti.kssr5.backend.keygen.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.keygen.dto.KeyGenDto;
import pl.edu.pg.eti.kssr5.backend.keygen.model.jpa.KeyGen;

@Component
public class CreateKeyGenExecutor extends BaseKeyGenExecutor{

    public Long createKeyGenForChild(KeyGenDto keyGenDto){
        KeyGen entity = new KeyGen();
        entity.setKey(keyGenDto.getKey());
        entity.setChild(childRepository.findOne(keyGenDto.getChildId()));
        return keyGenRepository.save(entity).getId();
    }

}
