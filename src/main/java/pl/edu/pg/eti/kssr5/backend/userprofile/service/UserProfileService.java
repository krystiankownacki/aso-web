package pl.edu.pg.eti.kssr5.backend.userprofile.service;

import pl.edu.pg.eti.kssr5.backend.userprofile.dto.LoginDto;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.UserDto;

public interface UserProfileService {

    LoginDto userAuthenticate(LoginDto loginDto);

    Long createUser(UserDto userToSave);
}
