package pl.edu.pg.eti.kssr5.backend.keygen.endpoint;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.edu.pg.eti.kssr5.backend.keygen.dto.KeyGenDto;

@RequestMapping("/keygen")
public interface KeyGenGateway {

    @RequestMapping(method = RequestMethod.POST, value = "/save")
    Long create(@RequestBody KeyGenDto keyGenDto);

    @RequestMapping(method = RequestMethod.POST, value = "/addChild/{key}/{id}")
    void addChildToParent(@PathVariable String key, @PathVariable Long parentId);
}
