package pl.edu.pg.eti.kssr5.backend.keygen.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.edu.pg.eti.kssr5.backend.keygen.model.jpa.KeyGen;

@Repository
public interface KeyGenRepository extends CrudRepository<KeyGen, Long>{

    KeyGen findByKeyAndActiveFlagTrue(String key);
}
