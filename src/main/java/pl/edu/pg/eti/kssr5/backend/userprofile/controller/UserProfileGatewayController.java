package pl.edu.pg.eti.kssr5.backend.userprofile.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.LoginDto;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.UserDto;
import pl.edu.pg.eti.kssr5.backend.userprofile.endpoint.UserProfileGateway;
import pl.edu.pg.eti.kssr5.backend.userprofile.service.UserProfileService;

@CrossOrigin
@RestController
public class UserProfileGatewayController implements UserProfileGateway {

    @Autowired
    private UserProfileService userProfileService;

    @Override
    public LoginDto userAuthenticate(@RequestBody LoginDto loginDto) {
        return userProfileService.userAuthenticate(loginDto);
    }

    @Override
    public Long registerUser(@RequestBody UserDto userDto) {
        return userProfileService.createUser(userDto);
    }
}
