package pl.edu.pg.eti.kssr5.backend.coordinate.executor;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Getter
public class CoordinateExecutor {

    @Autowired
    ReadCoordinateExecutor readExecutor;

    @Autowired
    CreateCoordinateExecutor createExecutor;
}
