package pl.edu.pg.eti.kssr5.backend.coordinate.executor;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.pg.eti.kssr5.backend.child.dao.ChildRepository;
import pl.edu.pg.eti.kssr5.backend.coordinate.dao.CoordinateRepository;

public abstract class BaseCoordinateExecutor {

    @Autowired
    CoordinateRepository coordinateRepository;

    @Autowired
    ChildRepository childRepository;

    ModelMapper modelMapper = new ModelMapper();
}
