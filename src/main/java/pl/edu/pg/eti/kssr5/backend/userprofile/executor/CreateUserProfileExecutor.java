package pl.edu.pg.eti.kssr5.backend.userprofile.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.userprofile.dto.UserDto;
import pl.edu.pg.eti.kssr5.backend.userprofile.model.jpa.UserProfile;

@Component
public class CreateUserProfileExecutor extends BaseUserProfileExecutor{

    public UserDto createUser(UserDto userDto){
        return modelMapper.map(repository.save(modelMapper.map(userDto, UserProfile.class)), UserDto.class);
    }
}
