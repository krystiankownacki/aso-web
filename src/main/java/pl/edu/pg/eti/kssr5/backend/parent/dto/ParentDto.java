package pl.edu.pg.eti.kssr5.backend.parent.dto;

import com.google.common.collect.Lists;
import lombok.Data;
import pl.edu.pg.eti.kssr5.backend.child.dto.ChildDto;

import java.io.Serializable;
import java.util.List;

@Data
public class ParentDto implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
    private List<ChildDto> childs = Lists.newArrayList();
}
