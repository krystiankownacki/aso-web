package pl.edu.pg.eti.kssr5.backend.coordinate.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import pl.edu.pg.eti.kssr5.backend.coordinate.dto.CoordinateDto;
import pl.edu.pg.eti.kssr5.backend.coordinate.endpoint.CoordinateGateway;
import pl.edu.pg.eti.kssr5.backend.coordinate.service.CoordinateService;

import java.util.List;

/**
 * Created by Krystian on 2017-06-06.
 */
@RestController
public class CoordinateGatewayController implements CoordinateGateway {

    private static final Logger logger = LoggerFactory.getLogger(CoordinateGatewayController.class);

    @Autowired
    private CoordinateService coordinateService;

    @Override
    public List<CoordinateDto> findAllCoordinatesByChild(@PathVariable Long childId){
        logger.info("Find coordinates by child: " + childId);
        List<CoordinateDto> result = coordinateService.findCoordinatesByChildId(childId);
        logger.info("Result: " + result.size());
        return result;
    }

    @Override
    public CoordinateDto postCoordinate(@RequestBody CoordinateDto coordinateDto) {
        logger.info("Trying to save coordinate for child = " + coordinateDto.getChildId() + " date: " + coordinateDto.getDate() + " " + coordinateDto.getTime());
        CoordinateDto result = coordinateService.save(coordinateDto);
        logger.info("Coordinates saved");
        return result;
    }

}
