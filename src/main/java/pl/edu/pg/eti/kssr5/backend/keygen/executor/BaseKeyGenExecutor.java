package pl.edu.pg.eti.kssr5.backend.keygen.executor;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.pg.eti.kssr5.backend.child.dao.ChildRepository;
import pl.edu.pg.eti.kssr5.backend.keygen.dao.KeyGenRepository;

public abstract class BaseKeyGenExecutor {

    @Autowired
    KeyGenRepository keyGenRepository;

    @Autowired
    ChildRepository childRepository;

    ModelMapper modelMapper = new ModelMapper();
}
