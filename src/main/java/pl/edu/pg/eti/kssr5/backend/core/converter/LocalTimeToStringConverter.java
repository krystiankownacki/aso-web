package pl.edu.pg.eti.kssr5.backend.core.converter;

import org.springframework.util.StringUtils;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;

@Converter
public class LocalTimeToStringConverter implements AttributeConverter<LocalTime, String>{

    private static final DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm");

    @Override
    public String convertToDatabaseColumn(LocalTime attribute) {
        if(attribute == null) {
            return null;
        } else {
            return attribute.format(TIME_FORMATTER);
        }
    }

    @Override
    public LocalTime convertToEntityAttribute(String dbData) {
        if(StringUtils.isEmpty(dbData)) {
            return null;
        } else {
            return LocalTime.parse(dbData, TIME_FORMATTER);
        }
    }
}
