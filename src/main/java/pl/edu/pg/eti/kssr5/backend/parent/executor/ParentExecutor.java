package pl.edu.pg.eti.kssr5.backend.parent.executor;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public class ParentExecutor {

    @Autowired
    ReadParentExecutor readExecutor;

    @Autowired
    CreateParentExecutor createExecutor;

    @Autowired
    UpdateParentExecutor updateExecutor;
}
