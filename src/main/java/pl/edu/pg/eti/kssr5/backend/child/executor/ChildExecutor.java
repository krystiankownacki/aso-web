package pl.edu.pg.eti.kssr5.backend.child.executor;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Getter
@Component
public class ChildExecutor {

    @Autowired
    CreateChildExecutor createExecutor;

    @Autowired
    ReadChildExecutor readExecutor;

    @Autowired
    UpdateChildExecutor updateExecutor;
}
