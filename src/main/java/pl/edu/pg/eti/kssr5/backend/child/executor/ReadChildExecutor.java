package pl.edu.pg.eti.kssr5.backend.child.executor;

import org.springframework.stereotype.Component;
import pl.edu.pg.eti.kssr5.backend.child.dto.ChildDto;

import java.util.List;
import java.util.stream.Collectors;

@Component
public class ReadChildExecutor extends BaseChildExecutor {

    public List<ChildDto> getChildrensByParentId(Long parentId){
        return repository.getAllByActiveFlagTrueAndParent_Id(parentId).stream().map(child -> mapper.map(child, ChildDto.class)).collect(Collectors.toList());
    }
}
