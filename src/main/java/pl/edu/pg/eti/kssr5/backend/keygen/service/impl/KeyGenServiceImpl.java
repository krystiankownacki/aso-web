package pl.edu.pg.eti.kssr5.backend.keygen.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pg.eti.kssr5.backend.child.executor.ChildExecutor;
import pl.edu.pg.eti.kssr5.backend.keygen.dto.KeyGenDto;
import pl.edu.pg.eti.kssr5.backend.keygen.executor.KeyGenExecutor;
import pl.edu.pg.eti.kssr5.backend.keygen.service.KeyGenService;

import javax.transaction.Transactional;

@Service
public class KeyGenServiceImpl implements KeyGenService {

    @Autowired
    KeyGenExecutor keyGenExecutor;

    @Autowired
    ChildExecutor childExecutor;

    @Override
    @Transactional
    public Long create(KeyGenDto keyGenDto) {
        return keyGenExecutor.getCreateExecutor().createKeyGenForChild(keyGenDto);
    }

    @Override
    @Transactional
    public void addChildToParent(String key, Long parentId) {
        childExecutor.getUpdateExecutor().addParentToChild(parentId, keyGenExecutor.getReadExecutor().getChildByKenGen(key).getId());
    }
}
