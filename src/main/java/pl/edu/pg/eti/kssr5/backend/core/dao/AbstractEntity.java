package pl.edu.pg.eti.kssr5.backend.core.dao;

import lombok.Data;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import pl.edu.pg.eti.kssr5.backend.core.converter.LocalDateToStringConverter;
import pl.edu.pg.eti.kssr5.backend.userprofile.model.jpa.UserProfile;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDate;

@Data
@MappedSuperclass
@EntityListeners({AuditingEntityListener.class, CustomAuditListener.class})
public abstract class AbstractEntity implements Serializable{

    @Column(name = "ACTIVE_FLAG")
    private Boolean activeFlag;

    @Column(name = "DESCRIPTION")
    private String description;

    @CreatedDate
    @Column(name = "DATE_CREATE")
    @NotNull
    @Convert(converter = LocalDateToStringConverter.class)
    private LocalDate dateCreate;

    @LastModifiedDate
    @Column(name = "LAST_MODIFIED_DATE")
    @NotNull
    @Convert(converter = LocalDateToStringConverter.class)
    private LocalDate lastModifiedDate;

    @CreatedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "USER_CRATE", referencedColumnName = "USERNAME")
    private UserProfile userCreate;

    @LastModifiedBy
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "LAST_MODIFIED_BY", referencedColumnName = "USERNAME")
    private UserProfile lastModifiedBy;
}
