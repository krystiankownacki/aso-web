package pl.edu.pg.eti.kssr5.backend.coordinate.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.edu.pg.eti.kssr5.backend.coordinate.dto.CoordinateDto;
import pl.edu.pg.eti.kssr5.backend.coordinate.executor.CoordinateExecutor;
import pl.edu.pg.eti.kssr5.backend.coordinate.service.CoordinateService;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class CoordinateServiceImpl implements CoordinateService {

    @Autowired
    CoordinateExecutor coordinateExecutor;

    @Override
    public List<CoordinateDto> findCoordinatesByChildId(Long childId) {
        return coordinateExecutor.getReadExecutor().getCoordinatesByChildId(childId);
    }

    @Override
    @Transactional
    public CoordinateDto save(CoordinateDto coordinateDto) {
        return coordinateExecutor.getCreateExecutor().save(coordinateDto);
    }
}
