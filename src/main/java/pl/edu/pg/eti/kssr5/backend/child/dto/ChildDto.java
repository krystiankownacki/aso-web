package pl.edu.pg.eti.kssr5.backend.child.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class ChildDto implements Serializable {
    private Long id;
    private String firstName;
    private String lastName;
}
