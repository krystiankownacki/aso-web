package pl.edu.pg.eti.kssr5.backend.userprofile.executor;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import pl.edu.pg.eti.kssr5.backend.child.dao.ChildRepository;
import pl.edu.pg.eti.kssr5.backend.parent.dao.ParentRepository;
import pl.edu.pg.eti.kssr5.backend.userprofile.dao.UserProfileRepository;

public abstract class BaseUserProfileExecutor {

    @Autowired
    UserProfileRepository repository;

    @Autowired
    ParentRepository parentRepository;

    @Autowired
    ChildRepository childRepository;

    ModelMapper modelMapper = new ModelMapper();
}
