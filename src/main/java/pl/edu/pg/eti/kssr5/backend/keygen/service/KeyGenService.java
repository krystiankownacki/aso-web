package pl.edu.pg.eti.kssr5.backend.keygen.service;

import pl.edu.pg.eti.kssr5.backend.keygen.dto.KeyGenDto;

public interface KeyGenService {

    Long create(KeyGenDto keyGenDto);

    void addChildToParent(String key, Long parentId);
}
